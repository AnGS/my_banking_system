from django.contrib import admin

from .models import BankAccountType, UserBankAccount, UserAddress


admin.site.register(BankAccountType)
admin.site.register(UserBankAccount)
admin.site.register(UserAddress)
