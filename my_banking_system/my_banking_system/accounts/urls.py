from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import home_page


from django.conf import settings
from django.conf.urls.static import static

app_name = 'accounts'

urlpatterns = [
    path('', home_page),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
